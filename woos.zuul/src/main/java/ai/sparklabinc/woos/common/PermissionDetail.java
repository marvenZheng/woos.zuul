package ai.sparklabinc.woos.common;

import java.io.Serializable;

public class PermissionDetail implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5104297712990625269L;

	// ID
	private Long id;
	// 名称
	private String name;
	// 描述
	private String description;
	// 资源URL
	private String url;
	// 资源行过滤条件
	private String rowsFilter;
	// 资源列过滤条件
	private String columnsFilter;

	private IsSelected isSelected;

	private boolean isData;
	
	private String tableName;
	
	private Long groupPermissionId;

	private Long userPermissionId;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getRowsFilter() {
		return rowsFilter;
	}

	public void setRowsFilter(String rowsFilter) {
		this.rowsFilter = rowsFilter;
	}

	public String getColumnsFilter() {
		return columnsFilter;
	}

	public void setColumnsFilter(String columnsFilter) {
		this.columnsFilter = columnsFilter;
	}

	public IsSelected getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(IsSelected isSelected) {
		this.isSelected = isSelected;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	public Long getGroupPermissionId() {
		return groupPermissionId;
	}

	public void setGroupPermissionId(Long groupPermissionId) {
		this.groupPermissionId = groupPermissionId;
	}
	
	public Long getUserPermissionId() {
		return userPermissionId;
	}

	public void setUserPermissionId(Long userPermissionId) {
		this.userPermissionId = userPermissionId;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PermissionDetail other = (PermissionDetail) obj;
		if (columnsFilter == null) {
			if (other.columnsFilter != null)
				return false;
		} else if (!columnsFilter.equals(other.columnsFilter))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (rowsFilter == null) {
			if (other.rowsFilter != null)
				return false;
		} else if (!rowsFilter.equals(other.rowsFilter))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	public static enum IsSelected {
		TRUE, FALSE,DISPLAY;
	}

	public boolean isData() {
		return isData;
	}

	public void setData(boolean isData) {
		this.isData = isData;
	}
	
	
}
