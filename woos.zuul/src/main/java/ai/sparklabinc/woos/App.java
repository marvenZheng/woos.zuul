package ai.sparklabinc.woos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import ai.sparklabinc.woos.filter.AuthenticationIntercept;

@EnableEurekaClient
@SpringBootApplication
@EnableZuulProxy
@EnableFeignClients
@ComponentScan(basePackages = { "ai.sparklabinc.woos" })
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
    
    @Bean  
    public AuthenticationIntercept authenticationInterceptFilter() {  
        return new AuthenticationIntercept();  
    }  
    
    @Bean
 	public WebMvcConfigurer webMvcConfigurer() {
 		return new WebMvcConfigurerAdapter() {
 			// 跨域请求控制
 			@Override
 			public void addCorsMappings(CorsRegistry registry) {
 				registry.addMapping("/**").allowedOrigins("*").allowCredentials(true).allowedHeaders("*")
 						.allowedMethods("GET", "OPTIONS", "POST", "PUT", "DELETE", "PATCH");
 			}

 		};
 	}
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
}
