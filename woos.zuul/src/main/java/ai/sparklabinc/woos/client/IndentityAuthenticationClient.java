package ai.sparklabinc.woos.client;

import java.util.Map;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import ai.sparklabinc.woos.api.SignInCommand;
/**
 * 调用identity服务
 * @author zhouwei
 *
 */
@FeignClient(name = "IDENTITY")
public interface IndentityAuthenticationClient {
	/**
	 * 用户登录
	 * @param username
	 * @param password
	 * @param request
	 * @return
	 */
	@PostMapping(value = "/authentication/sign-in")
	public Map<String,Object> signIn(@RequestBody SignInCommand command);
	/**
	 * 获取系统权限信息
	 * @return
	 */
	@PostMapping(value = "/authentication/system-permissions")
	public Map<String, Object> systemPermission();
	/**
	 * @title 获取用户的所有权限
	 * @param username
	 * @return
	 */
	@GetMapping(value = "/authentication/permission/{username}")
	public Map<String, Object> permissionsByUsername(@PathVariable("username") String username);
}
