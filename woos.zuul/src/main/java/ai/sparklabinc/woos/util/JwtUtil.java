package ai.sparklabinc.woos.util;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class JwtUtil {
	
	public static Claims  parseJwtToken(String token) throws Exception {
		String authenticationJwtKey = (String) EhcacheUtil.getValue("systemEhcache", "jwtAccessKey");
		// 对secretKey加密处理
		byte[] encodedKey = Base64.decodeBase64(authenticationJwtKey);
		SecretKey secretKey = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
		// 获取用户
		return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
	}
}
