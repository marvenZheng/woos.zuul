package ai.sparklabinc.woos.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.sparklabinc.woos.client.IndentityAuthenticationClient;
import ai.sparklabinc.woos.util.EhcacheUtil;
import ai.sparklabinc.woos.util.JwtUtil;
import io.jsonwebtoken.Claims;

@RestController
@RequestMapping(value = "/authentication")
public class AuthenticationController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationController.class);
	@Autowired
	private IndentityAuthenticationClient indentityAuthenticationClient;

	/**
	 * @title 用户登陆鉴权授权
	 * @param command
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "sign-in", method = RequestMethod.POST)
	@ResponseBody
	public Object signIn(@RequestBody SignInCommand command, HttpServletRequest request) throws Exception {
		Map<String, Object> resultMap = indentityAuthenticationClient.signIn(command);
		if (resultMap == null) {
			resultMap = new HashMap<>();
			resultMap.put("success", false);
			return resultMap;
		}

		resultMap.put("success", true);
		EhcacheUtil.setValue("userEhcache", command.getUsername(), resultMap);
		// 不返回JwtKey
		Map<String, Object> returnMap = new HashMap<>();
		returnMap.put("accessToken", resultMap.get("accessToken"));
		returnMap.put("username", command.getUsername());

		return returnMap;

	}

	/**
	 * @title 获取指定用户的menus
	 * @param username
	 * @param token
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "menus", method = RequestMethod.GET)
	@ResponseBody
	public Object menuByUsername(String username, String token, HttpServletResponse response) throws Exception {
		Map<String, Object> userAuthentication = (Map<String, Object>) EhcacheUtil.getValue("userEhcache", username);
		if (userAuthentication == null) {
			
			Claims claims = JwtUtil.parseJwtToken(token);
			if(claims.getExpiration().after(new Date())) {
				Map<String, Object> userPermissions = this.indentityAuthenticationClient.permissionsByUsername(username);
				return userPermissions.get("menu");
			}else {
				LOGGER.error("Connot find permission for user:" + username);
				throw new Exception("accessToken is overdue");
			}
			
		}
		return userAuthentication.get("menu");
	}

}
