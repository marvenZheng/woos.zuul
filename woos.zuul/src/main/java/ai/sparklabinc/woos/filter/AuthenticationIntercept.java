package ai.sparklabinc.woos.filter;

import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

import ai.sparklabinc.woos.client.IndentityAuthenticationClient;
import ai.sparklabinc.woos.constant.Constant;
import ai.sparklabinc.woos.util.EhcacheUtil;
import ai.sparklabinc.woos.util.JwtUtil;
import io.jsonwebtoken.Claims;

@SuppressWarnings("all")
public class AuthenticationIntercept extends ZuulFilter {
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationIntercept.class);

	@Autowired
	private IndentityAuthenticationClient indentityAuthenticationClient;

	@Override
	public Object run() {

		RequestContext ctx = RequestContext.getCurrentContext();
		Object originalRequestPath = ctx.get(FilterConstants.REQUEST_URI_KEY);
		System.out.println(originalRequestPath);
		HttpServletRequest request = ctx.getRequest();
		HttpServletResponse response = ctx.getResponse();
		String contextPath = request.getContextPath();
		// String remoteHost = request.getRemoteHost();
		String url = request.getRequestURI();
		ctx.addZuulRequestHeader(Constant.CUSTOM_DEFINITION_REQUEST_HEADER_NAME, "22222");
		// 不拦截
		if (Arrays.asList(Constant.NOT_INTERCEPT_URLS_BY_ZUUL).contains(url))
			return null;

		// 获取token
		String token = request.getParameter("token");
		Map<String, String[]> parameterMap = request.getParameterMap();
		if (token == null) {
			LOGGER.warn("Token is null by url :" + url);
			this.sendLoginPage(ctx, contextPath);
			return null;
		}
		try {
			Object value = EhcacheUtil.getValue("systemEhcache", "navs");
			LOGGER.info("system permissions :" + value);
			List<Map<String, Object>> SystemNavs = (List<Map<String, Object>>) EhcacheUtil.getValue("systemEhcache",
					"navs");

			Claims claims = JwtUtil.parseJwtToken(token);
			Date expirationAt = claims.getExpiration();// 过期时间
			String username = claims.getSubject();// 用户名
			// String issuer = claims.getIssuer();//ip控制

			LOGGER.info("username:" + username + ",expirationAt:" + expirationAt);
			Map<String, Object> userPermissions = (Map<String, Object>) EhcacheUtil.getValue("userEhcache", username);

			// 权限判断
			if (userPermissions == null) {
				userPermissions = indentityAuthenticationClient.permissionsByUsername(username);
				// 更新缓存
				EhcacheUtil.setValue("userEhcache", username, userPermissions);
			}

			Map<String, Object> urlPermissions = (Map<String, Object>) userPermissions.get("urlPermissions");

			// 不具备权限
			 if(!urlPermissions.containsKey(url)) {
			 sendErrorPage(response,contextPath);
			LOGGER.warn("no permission :" + url);
			 return null;
			 }

			Object isdata = urlPermissions.get(url);
			// if("TRUE".equals(isdata)) {
			// 获取用户的业务属性
			Map<String, Object> business = (Map<String, Object>) userPermissions.get("business");
			String estoreCodes = (String) business.get("estoreCodes");
			String brandIds = (String) business.get("brandIds");
			if (StringUtils.isNotBlank(estoreCodes)) {
				String[] estoreCodesArr = estoreCodes.split(",");
				for (String estoreCode : estoreCodesArr) {
					request.setAttribute("estoreCode", estoreCode);
					request.getServletContext().setAttribute("estoreCode", estoreCode);
				}
			}

			if (StringUtils.isNotBlank(brandIds)) {
				request.setAttribute("brandIds", brandIds);
			}
			if(business.keySet().size()>0) {
				response.sendRedirect("http://localhost:8764/dsis/data-sources-process/processed-content/details?modelUids=1003&estore_code=JD");
				return null;
			}
		
			// }
			
		//	 Object originalRequestPath = ctx.get(FilterConstants.REQUEST_URI_KEY);
		        //http://localhost:10000/demo/list/data
		        //-->/api/prefix/list/data
		        //String modifiedRequestPath = url+"?estore_code=2222";
		       // System.out.println(modifiedRequestPath);
		        StringBuffer append = request.getRequestURL().append("?estore_code=2222");
		        ctx.setRouteHost(new URL(append.toString()));
		        ctx.put(FilterConstants.REQUEST_URI_KEY, append.toString());
		        System.out.println(append);
		       // response.sendRedirect(append.toString());
		        
		} catch (Exception e1) {
			try {
				e1.printStackTrace();
				LOGGER.warn("token is disabled :" + url);
				this.sendLoginPage(ctx, contextPath);
				return null;
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}

		return null;
	}

	@Override
	public boolean shouldFilter() {
		return true;// 是否执行此过滤器
	}

	@Override
	public int filterOrder() {
		return FilterConstants.SEND_ERROR_FILTER_ORDER;// 权重,值越大,权重越小
	}

	@Override
	public String filterType() {
		/**
		 * 
		 * pre：可以在请求被路由之前调用 route：在路由请求时候被调用 post：在route和error过滤器之后被调用
		 * error：处理请求时发生错误时被调用
		 */
		 return FilterConstants.PRE_TYPE;
	}

	/**
	 * @title 跳转到没有权限页面
	 * @param response
	 * @param contentPath
	 */
	public void sendErrorPage(HttpServletResponse response, String contentPath) {
		try {
			response.sendRedirect(contentPath + Constant.REQUEST_URL_FOR_NO_PROMISSION_HINT);
		} catch (Exception e) {
			e.printStackTrace();
		}
		;
	}

	/**
	 * @title 跳转到登陆页面
	 * @param ctx
	 * @param contextPath
	 */
	public void sendLoginPage(RequestContext ctx, String contextPath) {
		ctx.setSendZuulResponse(false);
		ctx.setResponseBody("<script> window.parent.location.href='" + contextPath + Constant.REQUEST_URL_FOR_LOGIN_
				+ "';</script>");
		ctx.getResponse().setContentType("text/html;charset=UTF-8");
	}
}
