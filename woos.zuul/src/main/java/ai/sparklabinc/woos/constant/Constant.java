package ai.sparklabinc.woos.constant;

/**
 * @title zuul的常量类
 * @author zhouwei
 *
 */
public class Constant {
	//zuul的ip,端口
	public static final String ZUUL_CLIENT_WEB_PATH = "http://192.168.31.82:10002";
	//登陆页面地址
	public static final String REQUEST_URL_FOR_LOGIN_ = "/portal/login.html";
	//登陆portal没有权限页面
	public static final String REQUEST_URL_FOR_NO_PREMISSION_ = "/portal/no-permission.html";
	//菜单页面
	public static final String REQUEST_URL_FOR_MENU_PORTAL_ = "/portal/portal.html";
	//没有权限提示页面
	public static final String REQUEST_URL_FOR_NO_PROMISSION_HINT = "/portal/no_permission_hint.html";
	//不需要过滤的页面
	public static final String[] NOT_INTERCEPT_URLS_BY_ZUUL= {REQUEST_URL_FOR_LOGIN_,REQUEST_URL_FOR_NO_PREMISSION_,REQUEST_URL_FOR_MENU_PORTAL_};
	//自定义request消息头name
	public static final String CUSTOM_DEFINITION_REQUEST_HEADER_NAME = "DEFINITION-HEADER";
	//系统缓存ehcache名字
	public static final String SYSTEM_EHCACHE_NAME="systemEhcache";
	//用户缓存ehcache名字
	public static final String USER_EHCACHE_NAME="userEhcache";
	//从identity获取的用户token
	public static final String USER_ACCESS_TOKEN_IN_MAP_KEY="access_token";
	//从identity获取的用户菜单权限
	public static final String USER_MENU_PERMISSION_IN_MAP_KEY = "menu";
	//初始化时从identity获取的系统菜单
	public static final String SYSTEM_MENU_PERMISSION_IN_MAP_KEY = "navs";
	//初始化时获取系统的秘钥
	public static final String SYSTEM_JWT_ACCESS_KEY_IN_MAP_KEY = "jwtAccessKey";
	//初始化时获取系统所有权限
	public static final String SYSTEM_PERMISSION_IN_MAP_KEY = "permissions";
}
