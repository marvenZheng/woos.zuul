package ai.sparklabinc.woos.component;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ai.sparklabinc.woos.client.IndentityAuthenticationClient;
import ai.sparklabinc.woos.util.EhcacheUtil;

/**
 * @title 获取系统权限
 * @author zhouwei
 *
 */
@Component
public class DefaultPermissionComonent implements InitializingBean {
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultPermissionComonent.class);

	@Autowired
	private IndentityAuthenticationClient indentityAuthenticationClient;

	@Override
	public void afterPropertiesSet() throws Exception {
		Map<String, Object> systemPermissions = this.indentityAuthenticationClient.systemPermission();
		if (systemPermissions.get("navs") == null)
			throw new Exception("Connot find any system navs");
		if (systemPermissions.get("permissions") == null)
			throw new Exception("Connot find any system permissions");
		if(systemPermissions.get("jwtAccessKey")==null) {
			throw new Exception("Connot find jwt access key");
		}
		
		EhcacheUtil.setValue("systemEhcache", "jwtAccessKey", systemPermissions.get("jwtAccessKey"));
		EhcacheUtil.setValue("systemEhcache", "navs", systemPermissions.get("navs"));
		EhcacheUtil.setValue("systemEhcache", "permissions", systemPermissions.get("permissions"));
		LOGGER.info("initial System permissions success");
	}

}
